from setuptools import setup

setup(
    name="stupid_package",
    version="0.1",
    description="test",
    url="http://gitlab.com/flokno/simple-test",
    author="Florian Knoop",
    license="ISC",
    packages=["stupid_package"],
)
